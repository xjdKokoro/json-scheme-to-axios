(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('axios')) :
  typeof define === 'function' && define.amd ? define(['exports', 'axios'], factory) :
  (factory((global['json-schema-to-axios'] = {}),global.axios));
}(this, (function (exports,axios) { 'use strict';

  axios = axios && axios.hasOwnProperty('default') ? axios['default'] : axios;

  /**
   * scheme config for params type define
   *
   * @class SchemaConfig
   */
  var SchemaConfig = /** @class */ (function () {
      function SchemaConfig(options, axiosConfig) {
          this.options = options;
          this.axiosConfig = axiosConfig;
      }
      return SchemaConfig;
  }());

  function contains(array, item) {
      if (Array.prototype.includes)
          return array.includes(item);
      return !!~array.indexOf(item);
  }

  console.log(axios);
  var urlMethods = ['get', 'delete'];
  function generateAxiosRequest(options, config) {
      return function (params) {
          var method = options.method, url = options.url;
          var isUrlMethod = contains(urlMethods, method);
          if (!config)
              config = {};
          var axiosParams = [config];
          var axiosFunc = axios[method];
          if (isUrlMethod) {
              config.params = params;
          }
          else {
              axiosParams.unshift(params);
          }
          var finalAxiosFunc = isUrlMethod ? axiosFunc : axiosFunc;
          return finalAxiosFunc.apply(void 0, [url].concat(axiosParams));
      };
  }
  function configs2AxiosHOF(schemaConfig) {
      return generateAxiosRequest(schemaConfig.options, schemaConfig.axiosConfig);
  }
  function toAxios(dict) {
      var finalDict = {};
      Object.keys(dict).forEach(function (key) {
          finalDict[key] = configs2AxiosHOF(dict[key]);
      });
      return finalDict;
  }

  exports.SchemaConfig = SchemaConfig;
  exports.toAxios = toAxios;

  Object.defineProperty(exports, '__esModule', { value: true });

})));
