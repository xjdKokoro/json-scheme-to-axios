import * as sinon from 'sinon'
import * as moxios from 'moxios'
import { generateAxiosRequest } from '../src/generate'

describe('Generate', () => {
  beforeAll(() => {
    moxios.install()
  })
  afterAll(() => {
    moxios.unintall()
  })

  it('get request', async (done) => {
    moxios.stubRequest('/test?id=1', {
      status: 200,
      responseText: 'is get'
    })

    const request = generateAxiosRequest<{ id: number }>({
      method: 'get',
      url: '/test'
    })

    const onFulfilled = sinon.spy()

    request({ id: 1 }).then(onFulfilled)

    moxios.wait(() => {
      expect(onFulfilled.getCall(0).args[0].data).toBe('is get')
      done()
    })
  })

  it('post request', async (done) => {
    moxios.stubRequest('/test', {
      status: 200,
      responseText: 'is post'
    })

    const request = generateAxiosRequest<{ id: number }>({
      method: 'post',
      url: '/test'
    })

    const onFulfilled = sinon.spy()

    request({ id: 1 }).then(onFulfilled)

    moxios.wait(() => {
      const response = onFulfilled.getCall(0).args[0]
      expect(response.config.data.id).toBe(1)
      expect(response.data).toBe('is post')
      done()
    })
  })
})
