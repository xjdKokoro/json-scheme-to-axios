import * as sinon from 'sinon'
import axios from 'axios'
import * as moxios from 'moxios'

describe('axios requests', () => {
  beforeAll(() => {
    moxios.install()
  })
  afterAll(() => {
    moxios.unintall()
  })

  it('has correct return', async (done) => {
    moxios.stubRequest('/test?id=1', {
      status: 200,
      responseText: 'hi'
    })
    
    const onFulfilled = sinon.spy()
    axios.get('/test', { params: { id: 1 } }).then(onFulfilled)

    moxios.wait(() => {
      expect(onFulfilled.getCall(0).args[0].data).toBe('hi')
      done()
    })
  })
})
