import { AxiosRequestConfig } from 'axios'

// axios supports methods
type Method = 'get' | 'delete' | 'post' | 'put' | 'patch'

interface Options {
  method: Method,
  url: string
}
/**
 * scheme config for params type define
 *
 * @class SchemaConfig
 */
export default class SchemaConfig<T> {
  public responseDataInterface: T
  public options: Options
  public axiosConfig: AxiosRequestConfig

  constructor(options: Options, axiosConfig?) {
    this.options = options
    this.axiosConfig = axiosConfig
  }
}
