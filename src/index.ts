import SchemaConfig from './config-class'
import { toAxios } from './generate'

export {
  SchemaConfig,
  toAxios
}
