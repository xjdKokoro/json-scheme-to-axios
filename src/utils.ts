export function contains<T>(array: T[], item: T): boolean {
  if (Array.prototype.includes) return array.includes(item)
  return !!~array.indexOf(item)
}
