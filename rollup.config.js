import rollupTypescript from 'rollup-plugin-typescript2'

export default {
  input: 'src/index.ts',
  output: {
    name: 'json-schema-to-axios',
    file: 'dist/json-schema-to-axios.js',
    format: 'umd'
  },
  plugins: [
    rollupTypescript()
  ]
}
